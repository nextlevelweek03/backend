## Parser TS para JS
yarn add tsc
tsc

## Heroku Commands

# Video de Referência para Deploy no Heroku
https://www.youtube.com/watch?v=DVTceFeaAdc&ab_channel=AngeloLuz

# Acessar Banco de Dados
heroku pg:psql

# Acessar o servidor
heroku run bash
