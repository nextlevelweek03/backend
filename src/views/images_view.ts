import Image from '../models/Image'
import baseUrl from '../utils/baseUrl'

export default {
  render(image: Image) {
    return {
      id: image.id, 
      url: `${baseUrl}uploads/${image.path}`
    }
  },

  renderMany(images: Image[]) {
    return images.map(image => this.render(image))
  }
}