const baseUrl = process.env.TS_NODE_DEV ?
process.env.DEV_BASE_URL
:
process.env.PROD_BASE_URL

export default baseUrl