/*

Referencias:

* Para criar uma tabela:
yarn typeorm migration:create -n create_images

* Rodar as migrations das tabelas:
 yarn typeorm migration:run

* Visualizar Banco de Dados:
https://docs.beekeeperstudio.io/installation/#linux-installation

*/

import express from 'express'
import path from 'path'
import cors from 'cors'

import 'express-async-errors'

import './database/connection'

import routes from './routes'
import upload from './config/upload'
import errorHandler from './errors/handler'

const app = express()

app.use(cors())
app.use(express.json())
app.use(routes)
app.use('/uploads', express.static(path.join(__dirname, '..', 'uploads')))
app.use(errorHandler)

app.listen(process.env.PORT || 3333)