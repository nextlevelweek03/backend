import { getRepository } from 'typeorm'
import Orphanage from '../models/Orphanage'
import orphanageView from '../views/orphanages_view'

import * as Yup from 'yup'

import { Request, Response } from 'express'

export default {
  async index(request: Request, response: Response) {
    const orphanagesRepository = getRepository(Orphanage)

    const orphanages = await orphanagesRepository.find({
      relations: ['images']
    })

    return response.json(orphanageView.renderMany(orphanages))
  },

  async show(request: Request, response: Response) {
    
    const { id } = request.params

    const orphanagesRepository = getRepository(Orphanage)

    const orphanage = await orphanagesRepository.findOneOrFail(id, {
      relations: ['images']
    })

    return response.json(orphanageView.render(orphanage))
  },

  async create(request: Request, response: Response) {
    // {
    //   "name": "Lar do Menor",
    //   "latitude": 23.6028744,
    //   "longitude": -46.7406379,
    //   "about": "Cuidamos de crianças e ensinamos desde música até esportes.",
    //   "instructions": "Agende um horário para nos visitar.",
    //   "opening_hours": "Das 8h até as 18h",
    //   "open_on_weekends": true
    // }
    
    const {
      name,
      latitude,
      longitude,
      about,
      instructions,
      opening_hours,
      open_on_weekends,
    } = request.body
  
    const orphanagesRepository = getRepository(Orphanage)
  
    const requestImages = request.files as Express.Multer.File[]

    const images = requestImages.map(image => {
      return { path: image.filename }
    })

    const data = {
      name,
      latitude,
      longitude,
      about,
      instructions,
      opening_hours,
      open_on_weekends: open_on_weekends === "true",

      images
    }

    const schema = Yup.object().shape({
      name: Yup.string().required('Campo *name* é obrigatório.'),
      latitude: Yup.number().required('Campo *latitude* é obrigatório.'),
      longitude: Yup.number().required('Campo *longitude* é obrigatório.'),
      about: Yup.string().required('Campo *about* é obrigatório.').max(300),
      instructions: Yup.string().required('Campo *instructions* é obrigatório.'),
      opening_hours: Yup.string().required('Campo *opening_hours* é obrigatório.'),
      open_on_weekends: Yup.boolean().required('Campo *open_on_weekends* é obrigatório.'),

      images: Yup.array(
        Yup.object().shape({
          path: Yup.string().required()
        })
      )
    })

    await schema.validate(data, {
      // retorna todos os erros, não apenas o 1º encontrado
      abortEarly: false
    })

    const orphanage = orphanagesRepository.create( data )
  
    const result = await orphanagesRepository.save(orphanage)
  
    return response.status(201).json({data: result})
  }
}