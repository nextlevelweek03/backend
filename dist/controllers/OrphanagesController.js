"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
}
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Orphanage_1 = __importDefault(require("../models/Orphanage"));
const orphanages_view_1 = __importDefault(require("../views/orphanages_view"));
const Yup = __importStar(require("yup"));
exports.default = {
    async index(request, response) {
        const orphanagesRepository = typeorm_1.getRepository(Orphanage_1.default);
        const orphanages = await orphanagesRepository.find({
            relations: ['images']
        });
        return response.json(orphanages_view_1.default.renderMany(orphanages));
    },
    async show(request, response) {
        const { id } = request.params;
        const orphanagesRepository = typeorm_1.getRepository(Orphanage_1.default);
        const orphanage = await orphanagesRepository.findOneOrFail(id, {
            relations: ['images']
        });
        return response.json(orphanages_view_1.default.render(orphanage));
    },
    async create(request, response) {
        // {
        //   "name": "Lar do Menor",
        //   "latitude": 23.6028744,
        //   "longitude": -46.7406379,
        //   "about": "Cuidamos de crianças e ensinamos desde música até esportes.",
        //   "instructions": "Agende um horário para nos visitar.",
        //   "opening_hours": "Das 8h até as 18h",
        //   "open_on_weekends": true
        // }
        const { name, latitude, longitude, about, instructions, opening_hours, open_on_weekends, } = request.body;
        const orphanagesRepository = typeorm_1.getRepository(Orphanage_1.default);
        const requestImages = request.files;
        const images = requestImages.map(image => {
            return { path: image.filename };
        });
        const data = {
            name,
            latitude,
            longitude,
            about,
            instructions,
            opening_hours,
            open_on_weekends: open_on_weekends === "true",
            images
        };
        const schema = Yup.object().shape({
            name: Yup.string().required('Campo *name* é obrigatório.'),
            latitude: Yup.number().required('Campo *latitude* é obrigatório.'),
            longitude: Yup.number().required('Campo *longitude* é obrigatório.'),
            about: Yup.string().required('Campo *about* é obrigatório.').max(300),
            instructions: Yup.string().required('Campo *instructions* é obrigatório.'),
            opening_hours: Yup.string().required('Campo *opening_hours* é obrigatório.'),
            open_on_weekends: Yup.boolean().required('Campo *open_on_weekends* é obrigatório.'),
            images: Yup.array(Yup.object().shape({
                path: Yup.string().required()
            }))
        });
        await schema.validate(data, {
            // retorna todos os erros, não apenas o 1º encontrado
            abortEarly: false
        });
        const orphanage = orphanagesRepository.create(data);
        const result = await orphanagesRepository.save(orphanage);
        return response.status(201).json({ data: result });
    }
};
