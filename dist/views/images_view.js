"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
}
Object.defineProperty(exports, "__esModule", { value: true });
const baseUrl_1 = __importDefault(require("../utils/baseUrl"));
exports.default = {
    render(image) {
        return {
            id: image.id,
            url: `${baseUrl_1.default}uploads/${image.path}`
        };
    },
    renderMany(images) {
        return images.map(image => this.render(image));
    }
};
